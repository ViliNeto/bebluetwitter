# Beblue Twitter Test App (Android) - Vilibaldo Neto #

Essa aplicação foi feita com base nos requisitos exigidos e usada documentação fornecida em: [https://dev.twitter.com/rest/public](Link URL).

Para esse projeto foi utilizado o [Framework Fabric](https://fabric.io/) integrado com o Android Studio.
Fabric usa várias bibliotecas nativamente (Retrofit por exemplo), acelerando o processo de desenvolvimento.

Documentação disponível também no site do Dev Twitter: [https://dev.twitter.com/rest/public](Link URL).

## Funções da aplicação ##

* Autenticação com conta do Twitter
* Visualização de Timeline
* Publicação de Tweets
* Pesquisa nos tweets mundiais (Query)
* Curtir Tweets
* Compartilhar Tweets com diversos aplicativos (Share with ActionProvider)

## Organização da aplicação ##

A aplicação foi dividida em dois packages: UI e UTIL, onde UI são as classes responsáveis pela User Interface e UTIL responsável pelo modelo e ferramentas.
Como a IDE trata métodos síncronos e assíncronos nativamente, não houve a necessidade de classes Async para tratar.

**UI**

* SplashScreenActivity
    * Tela de abertura do aplicativo (1,5s)
* LoginTwitterActivity
    * Responsável por chamar método que loga usuário no Twitter e autoriza aplicação.
* TimelineActivity
    * Activity responsável por mostrar Timeline do usuário logado e chamar métodos *onClickNewTweet*: que envia novo tweet e *onOptionsItemSelected*: onde a opção de pesquisa (query) se encontra.
* TweetViewActivity
    * Mostra um tweet específico, clicado pelo usuário na *TimelineActivity*.

**UTIL**

* ApplicationUtils
    * Responsável por criar Dialogs e checar o estado da conexão (Online/Offline).
* CustomTweetTimelineAdapter
    * Adapter que estende *TweetTimelineListAdapter* e é responsável pela população da *ListView* mostrando os Tweets na *UI/TimelineActivity*.
* TwitterKeyCheck
    * Apesar de ser uma Activity, ela não infla nenhum layout, sendo apenas responsável pelo oAuth da aplicação nos servidores do Twitter.

## Telas da aplicação ##

**Splashscreen**

![Splashscreen.png](https://bitbucket.org/repo/Mxdx8k/images/2786676369-Splashscreen.png)

**Login com Twitter**

![login1.png](https://bitbucket.org/repo/Mxdx8k/images/3312113801-login1.png)

**Autorizar aplicação**

![login2.png](https://bitbucket.org/repo/Mxdx8k/images/3066091813-login2.png)

**User Timeline**

![timeline.png](https://bitbucket.org/repo/Mxdx8k/images/465408932-timeline.png)

**Pesquisa query**

![search.png](https://bitbucket.org/repo/Mxdx8k/images/4005992274-search.png)

**Resposta da pesquisa**

![searchtimeline.png](https://bitbucket.org/repo/Mxdx8k/images/2057352784-searchtimeline.png)

**Escrevendo Tweet**

![writingtweet.png](https://bitbucket.org/repo/Mxdx8k/images/2301016367-writingtweet.png)

**Visualizando Tweet**

![posttweetok.png](https://bitbucket.org/repo/Mxdx8k/images/3190727244-posttweetok.png)

**Selecionando Tweet**

![clicktweet.png](https://bitbucket.org/repo/Mxdx8k/images/2952862952-clicktweet.png)

**Clicando em Like (animação)**

![likecliking.png](https://bitbucket.org/repo/Mxdx8k/images/3881802069-likecliking.png)

**Clicando em Like (final animação)**

![likeclicked.png](https://bitbucket.org/repo/Mxdx8k/images/3764603950-likeclicked.png)

### Contato ###

Vilibaldo Neto - [LinkedIn](https://br.linkedin.com/in/vilineto)

Email - vilineto@gmail.com