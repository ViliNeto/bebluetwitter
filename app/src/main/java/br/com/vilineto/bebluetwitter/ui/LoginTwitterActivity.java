package br.com.vilineto.bebluetwitter.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import br.com.vilineto.bebluetwitter.R;
import br.com.vilineto.bebluetwitter.util.ApplicationUtils;

public class LoginTwitterActivity extends Activity {

    //Dedicated TwitterAPI (Fabric) Login button
    private TwitterLoginButton loginButton;
    //Global Application Utils
    private ApplicationUtils applicationUtils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_login);

        applicationUtils = new ApplicationUtils();

        //Checking internet connection
        if(applicationUtils.checkInternetStatus(this)){
            //Get active session, if so
            TwitterSession session = Twitter.getSessionManager().getActiveSession();

            //In case user is not logged in to the application
            if(session == null){
                twitterLoginButton();
            }
            //In case the user is logged in to the application!
            else{

                startActivity(new Intent(this, TimelineActivity.class));
                finish();
            }
        }
        else{
            //Open dialog warning user about the connection
            dialogInternetConnection();
        }

    }

    //Case the user is not logged in
    private void twitterLoginButton(){
        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        loginButton.setCallback(new Callback<TwitterSession>() {
            //If user logged in with success
            @Override
            public void success(Result<TwitterSession> result) {
                //Call next activity
                startActivity(new Intent(getApplication(), TimelineActivity.class));
                finish();
            }

            //If user didn't logged in correctly
            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });
    }

    //Get the result user log in
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginButton.onActivityResult(requestCode, resultCode, data);

    }

    //Open dialog to warn user of internet issue (Disconnected)
    private void dialogInternetConnection(){
        //Declare Dialog
        final Dialog dialogNoInternet;
        dialogNoInternet = applicationUtils.buildDialog(LoginTwitterActivity.this,R.layout.dialog_final);
        //Avoid clicks outside the dialog
        dialogNoInternet.setCanceledOnTouchOutside(false);
        dialogNoInternet.show();

        //Set texts at Dialog
        TextView dialogTitle = (TextView) dialogNoInternet.findViewById(R.id.txt_title_last_dialog);
        dialogTitle.setText(getResources().getText(R.string.txt_warning));
        TextView dialogLastDescription = (TextView) dialogNoInternet.findViewById(R.id.txt_description_last_dialog);
        dialogLastDescription.setText(getResources().getText(R.string.txt_no_internet));
        TextView dialogButton = (TextView) dialogNoInternet.findViewById(R.id.txt_cancel_comment);
        dialogButton.setText(getResources().getText(R.string.btn_close));


        //Fix dialog dimensions
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        final int width = metrics.widthPixels;
        final int height = metrics.heightPixels;
        dialogNoInternet.getWindow().setLayout((6 * width) / 7, (4 * height) / 8);
        //Cancel dialogDescription button
        RelativeLayout dialogButtoninternet = (RelativeLayout) dialogNoInternet.findViewById(R.id.btnCloseLastDialog);

        // if clicked in close option dismiss the dialogDescription
        dialogButtoninternet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNoInternet.dismiss();
                finish();
            }
        });
    }


}
