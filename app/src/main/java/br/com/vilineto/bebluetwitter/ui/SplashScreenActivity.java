package br.com.vilineto.bebluetwitter.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import br.com.vilineto.bebluetwitter.R;
import br.com.vilineto.bebluetwitter.util.TwitterKeyCheck;


public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Set application activity to fullscreen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Find the view of this class (Splash in this case)
        setContentView(R.layout.splash_screen);

        //Timer before call the next Activity
        Thread timerThread = new Thread(){
            public void run(){
                try{
                    //Waits 1.5 seconds before go to next activity
                    sleep(1500);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    //Calling LoginTwitterActivity
                    Intent intent = new Intent(SplashScreenActivity.this,TwitterKeyCheck.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

}