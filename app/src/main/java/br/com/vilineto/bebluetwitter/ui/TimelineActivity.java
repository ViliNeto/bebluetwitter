package br.com.vilineto.bebluetwitter.ui;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.UserTimeline;

import br.com.vilineto.bebluetwitter.R;
import br.com.vilineto.bebluetwitter.util.ApplicationUtils;
import br.com.vilineto.bebluetwitter.util.CustomTweetTimelineAdapter;

public class TimelineActivity extends ListActivity {

    //Globals
    private Dialog dialogDescription;
    private String newTweet = "";
    private Context context;
    private StatusesService statusesService;
    private String username;
    private boolean backButtonControl = false;
    private ApplicationUtils applicationUtils;
    private TextView textCounting;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline);
        //Setting up the context for views that will need it
        context = this;
        applicationUtils = new ApplicationUtils();

        //Check internet state (Connected)
        if(applicationUtils.checkInternetStatus(this)){
            //Get active session, if so
            TwitterSession session = Twitter.getSessionManager().getActiveSession();
            //Setup username for logged in user
            username = session.getUserName();

            //Set Title
            setTitle("@" + username);

            //Get TimelineActivity from user ready
            final UserTimeline userTimeline = new UserTimeline.Builder()
                    .screenName(username)
                    .build();
            //Populate the adapter
            CustomTweetTimelineAdapter customTweetTimelineAdapter = new CustomTweetTimelineAdapter(this, userTimeline);
            setListAdapter(customTweetTimelineAdapter);
        }
        //Close app in case of no internet connection
        else{
            dialogInternetConnection();
        }
    }

    @Override
    public void onBackPressed() {
        //Check internet connection
        if(applicationUtils.checkInternetStatus(this)){
            //From query to usertimeline
            if(backButtonControl){
                final UserTimeline userTimeline = new UserTimeline.Builder()
                        .screenName(username)
                        .build();
                //Call the adapter to refresh and repopulate the list
                CustomTweetTimelineAdapter customTweetTimelineAdapter = new CustomTweetTimelineAdapter(this, userTimeline);
                setListAdapter(customTweetTimelineAdapter);
                backButtonControl = false;
                //Set Title
                setTitle("@"+username);
            }
            else{
                finish();
            }
        }
        //Close app in case of no internet connection
        else{
            dialogInternetConnection();
        }

    }

    //In case to click the button to write a tweet (Update status Twitter[POST statuses/update])
    public void onClickNewTweet(View view) {
        //Using Twittercore
        TwitterApiClient twitterApiClient = Twitter.getApiClient();
        statusesService = twitterApiClient.getStatusesService();

        //<editor-fold desc="Creates a tweet dialog">
        dialogDescription = applicationUtils.buildDialog(TimelineActivity.this,R.layout.dialog_search);
        TextView tvDialog = (TextView) dialogDescription.findViewById(R.id.tv_title_dialog);
        tvDialog.setText(getResources().getString(R.string.txt_new_tweet));
        dialogDescription.show();


        //<editor-fold desc="Updates the view with word counting">
        textCounting = (TextView) dialogDescription.findViewById(R.id.tv_words_counting);

        final TextWatcher txwatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            //Updates char value
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textCounting.setText("#"+String.valueOf(s.length()));
            }

            public void afterTextChanged(Editable s) {
            }
        };
        //Find view with text from tweet
        editText =(EditText) dialogDescription.findViewById(R.id.edt_query);
        editText.addTextChangedListener(txwatcher);
        //</editor-fold>

        //Find view of confirm button
        RelativeLayout dialogButton = (RelativeLayout) dialogDescription.findViewById(R.id.btnConfirm);

        //<editor-fold desc="onClickListener to confirm the tweet">
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newTweet = editText.getText().toString();

                //<editor-fold desc="Control Tweet lenght and internet status before update status">
                if(newTweet.length() > 0 && newTweet.length() < 141 ) {
                    //Check internet status (Online)
                    if(applicationUtils.checkInternetStatus(context)) {
                        //Prepare tweet status update using only String message as parameter.
                        statusesService.update(newTweet, null, null, null, null, null, null, null, null, new Callback<Tweet>() {
                            @Override
                            public void success(Result<Tweet> result) {
                                Toast.makeText(getApplication(), getResources().getString(R.string.txt_tweeted), Toast.LENGTH_LONG).show();
                                //Get list ready
                                final UserTimeline userTimeline = new UserTimeline.Builder()
                                        .screenName(username)
                                        .build();
                                //Set Title
                                setTitle("@" + username);
                                //set list adapter
                                CustomTweetTimelineAdapter customTweetTimelineAdapter = new CustomTweetTimelineAdapter(context, userTimeline);
                                setListAdapter(customTweetTimelineAdapter);
                            }

                            @Override
                            public void failure(TwitterException exception) {
                                Toast.makeText(getApplication(), getResources().getString(R.string.txt_no_tweeted), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    //Internet Status (Offline)
                    else{
                        dialogInternetConnection();
                    }
                }
                //Warn user about the requirements to tweet!
                else{
                    Toast.makeText(getApplication(), getResources().getString(R.string.txt_tweet_limit_lenght), Toast.LENGTH_LONG).show();
                    dialogDescription.dismiss();
                }
                //</editor-fold>

                dialogDescription.dismiss();
            }
        });
        //</editor-fold>
        //</editor-fold>

    }

    //Case screens rotate
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
            //In case dialogDescription wasn't called yet.
            if(dialogDescription != null){
                int height = (int)(context.getResources().getDisplayMetrics().heightPixels*0.60);
                int width = (int)(context.getResources().getDisplayMetrics().widthPixels*0.90);
                dialogDescription.getWindow().setLayout(width, height);
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_timeline, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //If user click on search option
        if (id == R.id.action_search) {

            //<editor-fold desc="Search query on Twitter">
            //Creating a dialog
            dialogDescription = applicationUtils.buildDialog(TimelineActivity.this,R.layout.dialog_search);
            dialogDescription.show();

            RelativeLayout dialogButton = (RelativeLayout) dialogDescription.findViewById(R.id.btnConfirm);
            // if clicked in close option dismiss the dialogDescription
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText editText = (EditText) dialogDescription.findViewById(R.id.edt_query);
                    final SearchTimeline searchTimeline = new SearchTimeline.Builder()
                            .query(editText.getText().toString())
                            .build();
                    CustomTweetTimelineAdapter customTweetTimelineAdapter = new CustomTweetTimelineAdapter(context, searchTimeline);
                    //Refresh the adapter with query
                    setListAdapter(customTweetTimelineAdapter);
                    //Set Title
                    setTitle(getResources().getString(R.string.txt_query)+" "+editText.getText().toString());

                    dialogDescription.dismiss();
                    backButtonControl = true;
                }
            });
            //</editor-fold>
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Open dialog to warn user of internet issue (Disconnected)
    private void dialogInternetConnection(){
        //Declare Dialog
        final Dialog dialogNoInternet;
        dialogNoInternet = applicationUtils.buildDialog(TimelineActivity.this,R.layout.dialog_final);
        //Avoid clicks outside the dialog
        dialogNoInternet.setCanceledOnTouchOutside(false);
        dialogNoInternet.show();

        //Set texts at Dialog
        TextView dialogTitle = (TextView) dialogNoInternet.findViewById(R.id.txt_title_last_dialog);
        dialogTitle.setText(getResources().getText(R.string.txt_warning));
        TextView dialogLastDescription = (TextView) dialogNoInternet.findViewById(R.id.txt_description_last_dialog);
        dialogLastDescription.setText(getResources().getText(R.string.txt_no_internet));
        TextView dialogButton = (TextView) dialogNoInternet.findViewById(R.id.txt_cancel_comment);
        dialogButton.setText(getResources().getText(R.string.btn_close));


        //Fix dialog dimensions
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        final int width = metrics.widthPixels;
        final int height = metrics.heightPixels;
        dialogNoInternet.getWindow().setLayout((6 * width) / 7, (4 * height) / 8);
        //Cancel dialogDescription button
        RelativeLayout dialogButtoninternet = (RelativeLayout) dialogNoInternet.findViewById(R.id.btnCloseLastDialog);

        // if clicked in close option dismiss the dialogDescription
        dialogButtoninternet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogNoInternet.dismiss();
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
