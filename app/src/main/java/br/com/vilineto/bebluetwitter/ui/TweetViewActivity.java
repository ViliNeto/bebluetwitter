package br.com.vilineto.bebluetwitter.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetUtils;
import com.twitter.sdk.android.tweetui.TweetView;

import br.com.vilineto.bebluetwitter.R;

public class TweetViewActivity extends Activity {

    private long tweetId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tweet_viewer);

        //Receive the extra bundle from the other activity: Tweet Id
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tweetId = extras.getLong("tweetId");
        }

        //Find Root Layout
        final LinearLayout myLayout = (LinearLayout) findViewById(R.id.tweet_linear_layout);

        //Define view through root layout
        TweetView tweetView = (TweetView) myLayout.findViewById(R.id.tweet_view);

        //Load tweet based on its ID (@param long tweetid)
        TweetUtils.loadTweet(tweetId, new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> result) {
                myLayout.addView(new TweetView(TweetViewActivity.this, result.data));
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(TweetViewActivity.this,"Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, TimelineActivity.class));
        finish();
        super.onBackPressed();
    }
}
