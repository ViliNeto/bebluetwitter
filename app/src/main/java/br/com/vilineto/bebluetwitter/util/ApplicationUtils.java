package br.com.vilineto.bebluetwitter.util;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Window;

public class ApplicationUtils {

    //Create a custom dialog
    public Dialog buildDialog(Context context, int resourceId){

        final Dialog dialog = new Dialog(context);
        //Remove titlebar
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Find custom Layout
        dialog.setContentView(resourceId);

        int width = (int)(context.getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(context.getResources().getDisplayMetrics().heightPixels*0.60);

        dialog.getWindow().setLayout(width, height);


        return dialog;
    }

    //Check internet Status
    public boolean checkInternetStatus(Context context){
        final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            // notify user: you are online
            return true;
        } else {
            // notify user: you are not online
            return false;
        }
    }

}
