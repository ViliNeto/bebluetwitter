package br.com.vilineto.bebluetwitter.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import br.com.vilineto.bebluetwitter.ui.LoginTwitterActivity;
import io.fabric.sdk.android.Fabric;


public class TwitterKeyCheck extends Activity{

    //Application Key and Secret
    private static final String TWITTER_KEY = "12jNZdq5uIn0Yawh20Iwz4ny8";
    private static final String TWITTER_SECRET = "kHImc4WiY4afeKGkeU09SnoZqUZvZc88OCNKzeTRnWxN9TEP7q";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Auth on Twitter servers
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());
        //Go to next activity
        startActivity(new Intent(this, LoginTwitterActivity.class));
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

}
